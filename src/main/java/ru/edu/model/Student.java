package ru.edu.model;

import java.util.Date;
import java.util.UUID;

/**
 * Класс отражающий структуру хранимых в таблице полей.
 */
public class Student {

    /**
     * Первичный ключ.
     *
     * Рекомендуется генерировать его только внутри
     * StudentsRepositoryCRUD.create(),
     * иными словами до момента пока объект
     * не будет сохранен в БД, он не должен
     * иметь значение id.
     */
    private UUID id;

    /**
     * Имя.
     */
    private String firstName;

    /**
     * Фамилия.
     */
    private String lastName;

    /**
     * Дата рождения.
     */
    private Date birthDate;

    /**
     * Выпускник.
     */
    private boolean isGraduated;

    /**
     * Конструктор.
     */
    public Student() {

    }

    /**
     *
     * @param firstNametmp
     * @param lastNametmp
     * @param birthDatetmp
     * @param isGraduatedtmp
     */
    public Student(final String firstNametmp,
                   final String lastNametmp, final Date birthDatetmp,
                   final boolean isGraduatedtmp) {
        this.firstName = firstNametmp;
        this.lastName = lastNametmp;
        this.birthDate = birthDatetmp;
        this.isGraduated = isGraduatedtmp;
    }

    /**
     *
     * @return id
     */
    public UUID getId() {
        return id;
    }

    /**
     *
     * @return firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @return lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @return birthDate
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     *
     * @return isGraduated
     */
    public boolean isGraduated() {
        return isGraduated;
    }

    /**
     *
     * @param idtemp
     */
    public void setId(final UUID idtemp) {
        this.id = idtemp;
    }

    /**
     *
     * @param firstNametemp
     */
    public void setFirstName(final String firstNametemp) {
        this.firstName = firstNametemp;
    }

    /**
     *
     * @param lastNametemp
     */
    public void setLastName(final String lastNametemp) {
        this.lastName = lastNametemp;
    }

    /**
     *
     * @param birthDatetemp
     */
    public void setBirthDate(final Date birthDatetemp) {
        this.birthDate = birthDatetemp;
    }

    /**
     * Установить значение.
     * @param graduatedtemp
     */
    public void setGraduated(final boolean graduatedtemp) {
        this.isGraduated = graduatedtemp;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Student{"
                + "id=" + id
                + ", firstName='" + firstName + '\''
                + ", lastName='" + lastName + '\''
                + ", birthDate=" + birthDate
                + ", isGraduated=" + isGraduated
                + '}';
    }
}
