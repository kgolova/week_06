package ru.edu;

import ru.edu.model.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class StudentsRepositoryCRUDImpl implements StudentsRepositoryCRUD {

    /**
     * Строка заброса к БД.
     */
    public static final String INSERT_STUDENT =
            "INSERT INTO STUDENTS (id, first_name, last_name,"
                    + " birth_date, is_graduated) VALUES(?, ?, ?, ?, ?)";

    /**
     * Строка заброса к БД.
     */
    public static final String SELECT_BY_ID =
            "SELECT * FROM STUDENTS WHERE id = ?";

    /**
     * Строка заброса к БД.
     */
    public static final String REMOVE_BY_ID =
            "DELETE FROM STUDENTS WHERE id = ?";

    /**
     * Строка заброса к БД.
     */
    public static final String SELECT_ALL = "SELECT * FROM STUDENTS";

    /**
     * Строка заброса к БД.
     */
    public static final String UPDATE =
            "UPDATE STUDENTS SET first_name=?,last_name=?,"
                    + "birth_date=?,is_graduated=? WHERE id=?";

    /**
     * соединение с БД.
     */
    private Connection connection;

    /**
     * Конструктор.
     * @param connectiontmp
     */
    public StudentsRepositoryCRUDImpl(final Connection connectiontmp) {
        this.connection = connectiontmp;
    }

    /**
     * Создание записи в БД.
     * id у student должен быть null, иначе требуется вызов update.
     * id генерируем через UUID.randomUUID()
     *
     * @param student
     * @return сгенерированный UUID
     */
    @Override
    public UUID create(final Student student) {

        UUID id = UUID.randomUUID();
        int insertedRows = execute(INSERT_STUDENT, id.toString(),
                student.getFirstName(), student.getLastName(),
                student.getBirthDate(), student.isGraduated());
        if (insertedRows == 0) {
            throw new IllegalStateException("Not inserted");
        }
        return id;
    }

    /**
     * Получение записи по id из БД.
     *
     * @param id
     * @return Student
     */
    @Override
    public Student selectById(final UUID id) {
        List<Student> query = query(SELECT_BY_ID, id.toString());
        if (query.isEmpty()) {
            return null;
        }
        return query.get(0);
    }

    /**
     * Получение всех записей из БД.
     *
     * @return List<Student>
     */
    @Override
    public List<Student> selectAll() {
        List<Student> query = query(SELECT_ALL);
        if (query.isEmpty()) {
            return null;
        }
        return query;
    }

    /**
     * Обновление записи в БД.
     *
     * @param student
     * @return количество обновленных записей
     */
    @Override
    public int update(final Student student) {

        UUID id = student.getId();

        int updateRows = execute(UPDATE, student.getFirstName(),
                student.getLastName(), student.getBirthDate(),
                student.isGraduated(), id.toString());

        if (updateRows == 0) {
            throw new IllegalStateException("Not updated");
        }
        return updateRows;
    }

    /**
     * Удаление указанных записей по id.
     *
     * @param idList
     * @return количество удаленных записей
     */
    @Override
    public int remove(final List<UUID> idList) {

        int removeRows = 0;
        for (UUID uuid : idList) {
            int removeRowsTemp = execute(REMOVE_BY_ID, uuid.toString());
            removeRows += removeRowsTemp;
        }

        if (removeRows == 0) {
            throw new IllegalStateException("Not deleted");
        }

        return removeRows;
    }


    private int execute(final String sql, final Object... values) {
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            return statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    private List<Student> query(final String sql, final Object... values) {
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            ResultSet resultSet = statement.executeQuery();
            List<Student> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(map(resultSet));
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    private Student map(final ResultSet resultSet) throws SQLException {
        Student student = new Student();
        student.setId(UUID.fromString(resultSet.getString("id")));
        student.setFirstName(resultSet.getString("first_name"));
        student.setLastName(resultSet.getString("last_name"));
        student.setBirthDate(resultSet.getDate("birth_date"));
        student.setGraduated(resultSet.getBoolean("is_graduated"));
        return student;
    }
}
