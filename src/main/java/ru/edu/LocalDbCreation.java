package ru.edu;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public final class LocalDbCreation {

    /**
     *  Путь к БД.
     */
    public static final String JDBC_H_2_LOCAL_DB_DB =
            "jdbc:h2:./target/localDb/db";

    /**
     * Конструктор.
     */
    private LocalDbCreation() {

    }

    /**
     * psvm.
     * @param args
     * @throws SQLException
     */
    public static void main(final String[] args) throws SQLException {

        Connection connection =
                DriverManager.getConnection(JDBC_H_2_LOCAL_DB_DB, "sa", "");
    }

}

