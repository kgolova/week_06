package ru.edu.model;

import org.junit.Test;

import java.util.Date;
import java.util.UUID;

import static org.junit.Assert.*;


public class StudentTest {

    @Test
    public void testStudent() {

        Student student = new Student();
        student.setId(UUID.randomUUID());
        student.setFirstName("Test1");
        student.setLastName("Test2");
        student.setGraduated(true);

        Date date = new Date();

        student.setBirthDate(date);

        assertEquals("Test1",   student.getFirstName());
        assertEquals("Test2",   student.getLastName());
        assertEquals(date,              student.getBirthDate());
        assertTrue(student.isGraduated());


        Student newStudent = new Student("Test3", "Test4", date,false);

        assertEquals("Test3", newStudent.getFirstName());
        assertEquals("Test4", newStudent.getLastName());
        assertEquals(date, newStudent.getBirthDate());
        assertFalse(newStudent.isGraduated());

    }

}

