package ru.edu;


import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import ru.edu.model.Student;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import static org.junit.Assert.assertEquals;

import static ru.edu.LocalDbCreation.JDBC_H_2_LOCAL_DB_DB;


public class StudentsRepositoryCRUDTest {

    Student student1;
    Student student2;

    @Before
    public void setUp() {

        student1 = new Student("firstName1", "lastName1",  new Date(Instant.now().getEpochSecond()), false);
        student2 = new Student("firstName2", "lastName1",  new Date(Instant.now().getEpochSecond()), false);

    }


    @Test
    public void createTest() throws SQLException {

        StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(getConnection());


        UUID createdId = crud.create(student1);

        Student newStudent = crud.selectById(createdId);

        assertEquals(student1.getFirstName(), newStudent.getFirstName());

        assertEquals(createdId, newStudent.getId());

    }

    @Test
    public void updateTest() throws SQLException {

        StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(getConnection());

        UUID createdId = crud.create(student2);

        student2.setId(createdId);
        student2.setFirstName("Иван");
        student2.setLastName("Иванович");
        student2.setBirthDate(new Date(Instant.now().getEpochSecond()));
        student2.setGraduated(true);

        int updateRows = crud.update(student2);

        if (updateRows > 0) {
            Student newStudent = crud.selectById(createdId);
            assertEquals("Иван",     newStudent.getFirstName());
            assertEquals("Иванович", newStudent.getLastName());

        }

    }

    @Test
    public void testSelectAll() throws SQLException {

        StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(getConnection());

        UUID createdId = crud.create(student1);

        List<Student> allRows = crud.selectAll();

        assertNotNull(allRows);

    }

    @Test
    public void testRemove() throws SQLException {

        StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(getConnection());
        List<UUID> listUuids = new ArrayList<>();

        UUID createdId1 = crud.create(student1);
        UUID createdId2 = crud.create(student2);

        listUuids.add(createdId1);
        listUuids.add(createdId2);

        int removeRows = crud.remove(listUuids);

        assertEquals(2, removeRows);

    }
    @Test (expected = IllegalStateException.class)
    public void testRemoveException() throws SQLException {

        StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(getConnection());
        List<UUID> listUuids = new ArrayList<>();
        listUuids.add(UUID.randomUUID());

        crud.remove(listUuids);
    }


    private Connection getConnection() throws SQLException {

        return DriverManager.getConnection(JDBC_H_2_LOCAL_DB_DB, "sa", "");

    }

}